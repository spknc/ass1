require 'test_helper'

class PostIdsControllerTest < ActionController::TestCase
  setup do
    @post_id = post_ids(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:post_ids)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create post_id" do
    assert_difference('PostId.count') do
      post :create, post_id: { body: @post_id.body, title: @post_id.title }
    end

    assert_redirected_to post_id_path(assigns(:post_id))
  end

  test "should show post_id" do
    get :show, id: @post_id
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @post_id
    assert_response :success
  end

  test "should update post_id" do
    patch :update, id: @post_id, post_id: { body: @post_id.body, title: @post_id.title }
    assert_redirected_to post_id_path(assigns(:post_id))
  end

  test "should destroy post_id" do
    assert_difference('PostId.count', -1) do
      delete :destroy, id: @post_id
    end

    assert_redirected_to post_ids_path
  end
end
